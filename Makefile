SH=bash
all: parolier_booklet.pdf parolier.pdf oubli

%_booklet.pdf: %_booklet.tex %.pdf
	latexmk -xelatex $<
	rm $*_booklet.tex

# Those are evaluated at call-time, so it works. Sort of.
PDFPAGES=$$(pdfinfo $*.pdf | grep "Pages:" | awk '{print $$2}')
NUMPAGES=$$(python -c "print(($(PDFPAGES) + 3)//4 * 4)")

%_booklet.tex: %_booklet.template.tex
	sed "s/@NUMPAGES@/$(NUMPAGES)/g" $< > $@

%.pdf: %.tex $(wildcard chansons/*.tex)
	latexmk -xelatex $<

.PHONY: clean upload oubli

clean:
	rm -f *.aux *.pdf *.fls *.log *.xdv *.fdb_latexmk *_booklet.tex

upload: parolier.pdf parolier_booklet.pdf
	scp $^ voxernestorum@sas.eleves.ens.fr:~/www

oubli:
	@for file in chansons/*.tex; do \
		[ "$$(basename "$$file")" = "_template.tex" ] && continue ; \
		grep -q "$$(basename "$$file" .tex)" parolier.tex || \
		echo "[ATTENTION] Oublié : $$(basename "$$file" .tex)" ;\
	done
